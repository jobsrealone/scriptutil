﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptUtil.Runtime
{
    public abstract class MyScriptAbstract : MonoBehaviour
    {
        public virtual void MyFunction() {

        }
    }
}
